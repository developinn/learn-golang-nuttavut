package main

import "fmt"
import "strconv"

func main() {
	fmt.Println("Please input whatever number.")
	var input int
	fmt.Scanln(&input)

	// LOOP
	for i:=1; i<=10; i++ {
		fmt.Printf(strconv.Itoa(i) + " ")
	}
	fmt.Println()

	// IF-ELSE
	if input%2 == 0 {
		fmt.Println("The number It is even.")
	} else {
		fmt.Println("The number is odd.")
	}

	// SWITCH-CASE
	switch input%4 {
		case 0: fmt.Println("The number is class 1.")
		case 1: fmt.Println("The number is class 2.")
		case 2: fmt.Println("The number is class 3.")
		case 3: fmt.Println("The number is class 4.")
	}

	var pause string
	fmt.Scanln(&pause)
}
