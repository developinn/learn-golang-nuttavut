package main

import "fmt"

func main() {
	sittinut := human{name: "Sittinut", age: 14}
	setJuvenile(&sittinut)
	fmt.Println(sittinut)

	variya := human{name: "Variya", age: 25}
	setJuvenile(&variya)
	fmt.Println(variya)
}

type human struct {
	name       string
	age        int
	isJuvenile bool
}

func setJuvenile(h *human) {
	h.isJuvenile = h.age < 18
}
