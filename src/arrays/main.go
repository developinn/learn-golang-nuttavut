package main

import "fmt"

func main() {
	var names []string
	names = append(names, "Sittinut")
	names = append(names, "Variya")
	names = append(names, "Manse")
	names = append(names, "Prayut")
	fmt.Println(names)

	surnames := []string{"Chivakunakorn", "Leelawattananusorn", "Song", "Chan-o-cha"}
	fmt.Println(surnames)

	for idx, name := range names {
		fmt.Println(idx+1, name, surnames[idx])
	}
}
