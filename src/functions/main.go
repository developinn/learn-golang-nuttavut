package main

import (
	"errors"
	"fmt"
	"os"
)

func main() {
	printFullName("Sittinut", "Chivakunakorn")
	fmt.Println(getFullName("Sittinut", "Chivakunakorn"))
	fmt.Println("- - - -")

	result, err := divide(5, 3)
	fmt.Println(result, err)
	if err != nil {
		os.Exit(1)
	}

	result, err = divide(5, 0)
	fmt.Println(result, err)
	if err != nil {
		os.Exit(1)
	}
}

func printFullName(firstName string, lastName string) {
	fmt.Println(firstName + " " + lastName)
}

func getFullName(firstName string, lastName string) string {
	return firstName + " " + lastName
}

func divide(dividend float32, divisor float32) (float32, error) {
	if divisor == 0.0 {
		err := errors.New("divided by zero")
		return 0, err
	}

	return dividend / divisor, nil
}
