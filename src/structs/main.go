package main

import (
	"fmt"
	"strconv"
)

func main() {
	sittinut := human{name: "Sittinut", weight: 72}
	sittinut.display()
	variya := human{name: "Variya", weight: 57}
	variya.display()
}

type human struct {
	name   string
	weight int
}

func (h human) display() {
	fmt.Println("My name is " + h.name + ". My weight is " + strconv.Itoa(h.weight) + ".")
}
